package org.example;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import io.github.cdimascio.dotenv.Dotenv;


public class Main {

    public static void FindIssues(){
        try{
            Dotenv dotenv = Dotenv.load();
            String projectName = dotenv.get("PROJECT_NAME");
            String token = dotenv.get("API_TOKEN");
            String yourURL = dotenv.get("ATLASSIAN_URL");
            String yourEmail = dotenv.get("USER_EMAIL");

            HttpResponse<com.mashape.unirest.http.JsonNode> response = Unirest.get
                            (yourURL + "/rest/api/3/search?jql=project=" + projectName + "&fields=key,summary")
                    .basicAuth(yourEmail, token)
                    .header("Accept", "application/json")
                    .asJson();
            if(response.getStatus() == 200) {
                String node = response.getBody().toString();
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(node);

                int size = jsonNode.get("issues").size();
                for (int i = 0; i < size; i++) {
                    System.out.print("Numer zgłoszenia: " + jsonNode.get("issues").get(i).get("key").asText());
                    System.out.println(" Nazwa zgłoszenia: " + jsonNode.get("issues").get(i).get("fields").get("summary").asText());
                }
            }else System.out.println("Error " + response.getStatus());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }

    }

    public static void main(String[] args) {
        FindIssues();

        }
    }
